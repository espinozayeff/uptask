const express = require('express')
const router = express.Router()

const projectsController = require('../controllers/projectsController')

module.exports = () => {
    router.get('/', projectsController.projectsHome)
    router.get('/new-project', projectsController.projectForm)
    router.post('/new-project', projectsController.newProject)
    return router
}