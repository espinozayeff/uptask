exports.projectsHome = (req, res) => res.render('index', {
    pageName: 'Projects'
})

exports.projectForm = (req, res) => res.render('newProject', {
    pageName: 'New project'
})

exports.newProject = (req, res) => console.log(req.body)